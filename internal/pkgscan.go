package internal

import (
	"io/ioutil"
	"path/filepath"
)

// Scan all rhel packages and return a full list of package names in the form of "repo\pkgname.rpm"
func ScanRhelPackages(root string, repos []string) []string {
	var ret []string

	for _, repo := range repos {
		p := GetRhelPkgDir(root, repo, "")
		files := scandir(repo, p)
		ret = append(ret, files...)
	}

	return ret
}

func scandir(repo, p string) []string {
	var ret []string

	fl, err := ioutil.ReadDir(p)
	if err != nil {
		return ret
	}

	for _, f := range fl {
		if f.IsDir() {
			ret = append(ret, scandir(repo, filepath.Join(p, f.Name()))...)
			continue
		}

		ret = append(ret, filepath.Join(repo, f.Name()))
	}

	return ret
}
