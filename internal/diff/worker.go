package diff

import (
	"log"

	"gitlab.com/jaytaph/repodiff/internal"
)

var RhelPkgList []string

// PackageWorker is the main worker that deals with incoming RPM packages. More of these will run in separate threads.
func PackageWorker(output *JsonOutputType, rootDir string, rpmCh chan internal.RpmType) {
	for {
		rpm := fetchFromQueue(rpmCh)
		if rpm == nil {
			return
		}

		// @TODO: we should have like a --filter argument for this?
		// if !strings.HasPrefix(rpm.FullPkgName, "squid") {
		// 	continue
		// }

		log.Printf("processing %s\n", rpm.FullPkgName)
		process(output, rootDir, *rpm)
	}
}

func process(output *JsonOutputType, rootDir string, rpm internal.RpmType) {
	// Generate on-disk path to rhel and c8 packages based on this rpm
	rhelPath, err := internal.FindRhelPkgPath(rootDir, rpm.FullPkgName)
	if err != nil {
		// output.AddAddedPackage(rpm.Repo, rpm.FullPkgName)
		// return
	}

	c8Path := internal.GetC8PkgPath(rootDir, rpm.Repo, rpm.FullPkgName)

	// If it's a module, the pkg name between rhel and c8 are different and we need to figure out the actual name first
	if rpm.IsModule {
		moduleName, err := internal.FindMatchingModuleVersion(rpm, RhelPkgList)
		if err != nil {
			output.AddMissingModulePackage(rpm.Repo, rpm.FullPkgName)
			log.Printf("cannot find rhel module for %s", rpm.FullPkgName)
			return
		}
		rhelPath, err = internal.FindRhelPkgPath(rootDir, moduleName)
		if err != nil {
			output.AddAddedPackage(rpm.Repo, rpm.FullPkgName)
			return
		}
	}

	// File found on both sides, diff the RPMS
	dlt, err := DiffRpm(rhelPath, c8Path)
	if err != nil {
		return
	}

	// Add info to our json if it has any differences
	if dlt.HasChanges() {
		output.AddDiff(rpm.Repo, rpm.FullPkgName, *dlt)
	}
}

// Return rpm from queue, or nil when the queue is empty
func fetchFromQueue(rpmCh chan internal.RpmType) *internal.RpmType {
	rpm, more := <-rpmCh
	if !more {
		return nil
	}

	return &rpm
}
