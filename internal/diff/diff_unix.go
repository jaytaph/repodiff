// +build !windows

package diff

import (
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
)

func DiffText(srcBytes, dstBytes []byte) []byte {
	srcTmpFile, _ := ioutil.TempFile(os.TempDir(), "")
	dstTmpFile, _ := ioutil.TempFile(os.TempDir(), "")

	defer func() {
		_ = os.Remove(srcTmpFile.Name())
		_ = os.Remove(dstTmpFile.Name())
	}()

	_ = ioutil.WriteFile(srcTmpFile.Name(), srcBytes, 0644)
	_ = ioutil.WriteFile(dstTmpFile.Name(), dstBytes, 0644)

	// It's important to run the diff in the TEMP directory, otherwise diff cannot find the files
	src := filepath.Base(srcTmpFile.Name())
	dst := filepath.Base(dstTmpFile.Name())

	diffCmd := exec.Command("/usr/bin/diff", src, dst)
	diffCmd.Dir = os.TempDir()
	diff, _ := diffCmd.Output()

	return diff

}
