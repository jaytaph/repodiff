package diff

import (
	"encoding/base64"
	"errors"
	"io"
	"io/ioutil"
	"os"

	"github.com/sassoftware/go-rpmutils"
)

type DiffStructType struct {
	Added    []string
	Removed  []string
	Modified map[string]string
}

func (dst *DiffStructType) HasChanges() bool {
	return len(dst.Added)+len(dst.Removed)+len(dst.Modified) > 0
}

func DiffRpm(src, dst string) (*DiffStructType, error) {
	var ds = DiffStructType{}
	var m map[string]string

	ds.Removed, ds.Modified = diff(src, dst)
	ds.Added, m = diff(dst, src)

	if len(ds.Modified) != len(m) {
		// Uh? src-dst and dst-src have different modified files?
		panic("modified error")
	}

	return &ds, nil
}

func diff(src, dst string) ([]string, map[string]string) {
	srcRpm, srcFileHandle, err := openRpm(src)
	defer func() {
		if srcFileHandle != nil {
			_ = srcFileHandle.Close()
		}
	}()
	if err != nil {
		return nil, nil
	}
	dstRpm, dstFileHandle, err := openRpm(dst)
	defer func() {
		if dstFileHandle != nil {
			_ = dstFileHandle.Close()
		}
	}()
	if err != nil {
		return nil, nil
	}

	srcFiles, err := srcRpm.Header.GetFiles()
	if err != nil {
		return nil, nil
	}

	dstFiles, err := dstRpm.Header.GetFiles()
	if err != nil {
		return nil, nil
	}

	addedList := make([]string, 0)
	modifiedList := make(map[string]string, 0)

	// Iterate all files
	for _, srcFile := range srcFiles {
		fileFound := false
		fileMatches := false
		for _, dstFile := range dstFiles {
			if srcFile.Name() == dstFile.Name() {
				fileFound = true
				fileMatches = srcFile.Size() == dstFile.Size() && srcFile.Digest() == dstFile.Digest()
				break
			}
		}

		if !fileFound {
			addedList = append(addedList, srcFile.Name())
			continue
		}
		if !fileMatches {
			srcBytes, _ := findFileInRpm(srcRpm, srcFile.Name())
			dstBytes, _ := findFileInRpm(dstRpm, srcFile.Name())

			diff := DiffText(srcBytes, dstBytes)

			modifiedList[srcFile.Name()] = base64.StdEncoding.EncodeToString(diff)
			continue
		}
	}

	// return added and modified files
	return addedList, modifiedList
}

func findFileInRpm(rpm *rpmutils.Rpm, name string) ([]byte, error) {
	r, err := rpm.PayloadReader()
	if err != nil {
		return nil, err
	}

	for {
		hdr, err := r.Next()
		// End of archive
		if err != nil {
			break
		}

		// Not the correct filename
		if hdr.Filename() != name {
			continue
		}

		// Found the correct file, add it to diff
		return ioutil.ReadAll(r)
	}

	return nil, errors.New("not found")
}

func openRpm(p string) (*rpmutils.Rpm, io.Closer, error) {
	f, err := os.Open(p)
	if err != nil {
		return nil, nil, err
	}

	rpm, err := rpmutils.ReadRpm(f)
	return rpm, f, err
}
