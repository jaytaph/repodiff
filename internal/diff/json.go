package diff

import (
	"sync"
)

type JsonOutputRepoType struct {
	AddedPackages  []string                  // Packages available on the C8, but not on the RHEL
	MissingModules []string                  // Modules that could not find a match on RHEL
	Diffs          map[string]DiffStructType // Differences between the packages
}

type JsonOutputType struct {
	m     sync.Mutex
	Repos map[string]JsonOutputRepoType // Different repos (appstream, baseos)
}

func NewJsonOutput() JsonOutputType {
	return JsonOutputType{
		Repos: make(map[string]JsonOutputRepoType, 0),
	}
}

func (j *JsonOutputType) AddDiff(repo, pkg string, diff DiffStructType) {
	j.m.Lock()
	defer j.m.Unlock()

	j.CheckRepoExists(repo)
	j.Repos[repo].Diffs[pkg] = diff
}

func (j *JsonOutputType) AddMissingModulePackage(repo, pkg string) {
	j.m.Lock()
	defer j.m.Unlock()

	j.CheckRepoExists(repo)

	t := j.Repos[repo]
	t.MissingModules = append(j.Repos[repo].MissingModules, pkg)
	j.Repos[repo] = t
}

func (j *JsonOutputType) AddAddedPackage(repo, pkg string) {
	j.m.Lock()
	defer j.m.Unlock()

	j.CheckRepoExists(repo)

	t := j.Repos[repo]
	t.AddedPackages = append(j.Repos[repo].AddedPackages, pkg)
	j.Repos[repo] = t
}

// Add repo if not exist
func (j *JsonOutputType) CheckRepoExists(repo string) {
	_, ok := j.Repos[repo]
	if ok {
		return
	}

	r := JsonOutputRepoType{
		AddedPackages:  make([]string, 0),
		MissingModules: make([]string, 0),
		Diffs:          make(map[string]DiffStructType, 0),
	}

	j.Repos[repo] = r
}
