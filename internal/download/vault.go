package download

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

// The default vault URL. Uses %s to fill in the actual repos
const C8VaultUrl = "https://vault.centos.org/8.3.2011/%s/Source/SPackages"

var C8Repos = []string{"appstream", "baseos", "centosplus", "extras", "highavailability", "powertools"}

// VaultType is a simple abstraction of the C8 vault server
type VaultType struct {
	BaseUrl string
}

// Check if a package is present on the server. Will return if so, which repo and the expected length of the package.
// Uses a HEAD request to fetch info for quick return.
func (v *VaultType) GetPackageInfo(repo, name string) (bool, int64) {
	client := http.Client{
		Timeout: 5 * time.Second,
	}

	url := fmt.Sprintf(v.BaseUrl+"/%s", repo, name)

	resp, err := client.Head(url)
	if err != nil {
		return false, 0
	}

	if resp.StatusCode == 200 {
		return true, resp.ContentLength
	}

	return false, 0
}

// DownloadPackage will return a reader that can be used to read the package from the server.
func (v *VaultType) DownloadPackage(repo, name string) (io.ReadCloser, error) {
	client := http.Client{}

	url := fmt.Sprintf(v.BaseUrl+"/%s", repo, name)

	log.Printf("downloading from: %s\n", url)
	resp, err := client.Get(url)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == 200 {
		return resp.Body, nil
	}

	return nil, errors.New("package not found")
}

// GetIndices will return a map of all repo's and the containing packages.
func (v *VaultType) GetIndices() (map[string]string, error) {
	client := http.Client{}

	ret := make(map[string]string)

	for _, r := range C8Repos {
		url := fmt.Sprintf(v.BaseUrl+"/", r)

		resp, err := client.Get(url)
		if err != nil {
			return nil, err
		}
		if resp.StatusCode != 200 {
			return nil, fmt.Errorf("invalid status code: %d", resp.StatusCode)
		}

		b, err := ioutil.ReadAll(resp.Body)
		defer func() {
			_ = resp.Body.Close()
		}()

		if err != nil {
			return nil, err
		}

		ret[r] = string(b)
	}

	return ret, nil
}
