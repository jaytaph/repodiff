package internal

import (
	"errors"
	"os"
	"path/filepath"
	"strings"
)

// Different repositories
var C8RepoDirs = []string{
	"BaseOS",
	"AppStream",
	"HighAvailability",
	"PowerTools",
	"centosplus",
	"extras",
}

var RhelRepoDirs = []string{
	"rhel-8-for-x86_64-baseos-source-rpms",
	"rhel-8-for-x86_64-appstream-source-rpms",
	"rhel-8-for-x86_64-codeready-source-rpms",
	"rhel-8-for-x86_64-supplementary-source-rpms",
}

// Browse all rhel repositories to find the pkg
func FindRhelPkgPath(root, pkg string) (string, error) {
	for _, repo := range RhelRepoDirs {
		p := GetRhelPkgPath(root, repo, pkg)
		_, ok := os.Stat(p)
		if ok == nil {
			return p, nil
		}
	}

	return "", errors.New("not found")
}

// Return the directory name of the package in the rhel repo
func GetRhelPkgDir(root, repo, pkg string) string {
	prefixDir := ""
	if pkg != "" {
		prefixDir = strings.ToLower(string(pkg[0]))
	}

	return filepath.Join(root, "rhel8", repo, "Packages", prefixDir)
}

// Return the full rpm pkg name of the package in the rhel repo
func GetRhelPkgPath(root, repo, pkg string) string {
	return filepath.Join(GetRhelPkgDir(root, repo, pkg), pkg)
}

// Return the full dir name of the repo in the c8 repo
func GetC8PkgDir(root, repo string) string {
	repoBase := strings.ToLower(repo)

	return filepath.Join(root, "c8", repoBase)
}

// Return the full rpm pkg name of the package in the c8 repo
func GetC8PkgPath(root, repo, pkg string) string {
	return filepath.Join(GetC8PkgDir(root, repo), pkg)
}
