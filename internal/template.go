package internal

const IndexTemplate = `
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Rocky Linux - Rebranding effort</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <style>
        body {
            padding-top: 5rem;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="#">Rocky Linux - Rebranding effort</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </nav>

    <main role="main" class="container">

        <span class="float-right"><small>Last update {{ .timestamp }}</small></span>

{{ range $repo, $list := .repos }}
        <h2>{{ $repo }}</h2>

        <table class="table table-striped table-sm table-hover table-bordered">
            <thead>
                <tr><th>Added packages ({{ len $list.AddedPackages }})<br><small>These packages are available on C8, but not on RHEL8. This might be because of the fact they are module packages, or that some packages have a slight name change.</small></th></tr>
            </thead>
            <tbody>
            <tr><td>
                <a class="btn-link" data-toggle="collapse" href="#added_{{ $repo }}" role="button" aria-expanded="false" aria-controls="added_{{ $repo }}">
                    Click to view &raquo;
                </a>
                <div class="collapse" id="added_{{ $repo }}">
                    <div class="card card-body">
                        <table class="table table-sm">
                        {{ range $list.AddedPackages }}
                            <tr><td>{{ . }}</td></tr>
                        {{ end }}
                        </table>
                    </div>
                </div>
            </td></tr>
            </tbody>
        </table>

    <table class="table table-striped table-sm table-hover table-bordered">
        <thead>
            <tr><th>Unmatches module packages ({{ len $list.MissingModules }})<br><small>These packages are module packages, but we could not find a match on RHEL8.</small></th></tr>
        </thead>
        <tbody>
        <tr><td>
            <a class="btn-link" data-toggle="collapse" href="#missing_{{ $repo }}" role="button" aria-expanded="false" aria-controls="missing_{{ $repo }}">
                Click to view &raquo;
            </a>
            <div class="collapse" id="missing_{{ $repo }}">
                <div class="card card-body">
                    <table class="table table-sm">
                    {{ range $list.MissingModules }}
                        <tr><td>{{ . }}</td></tr>
                    {{ end }}
                    </table>
                </div>
            </div>
        </td></tr>
        </tbody>
    </table>


        <table class="table table-striped table-sm table-hover table-bordered">
            <thead>
                <tr><th colspan="2">Changed packages ({{ len $list.Diffs }})<br><small>These packages are modified on C8. This is mostly rebranding and debranding.</small></th></tr>
            </thead>
            <tbody>


{{ range $pkg,$diff := $list.Diffs }}
                <tr><td>

                    <a class="btn-link" data-toggle="collapse" href="#c_{{ $repo }}_{{ $pkg | sha }}" role="button" aria-expanded="false" aria-controls="c_{{ $repo }}_{{ $pkg | sha }}">
                        {{ $pkg }} &raquo;
                    </a>
                    <div class="collapse" id="c_{{ $repo }}_{{ $pkg | sha }}">
                        <div class="card card-body">

{{ if $diff.Added }}
                            <table class="table">
                                <tr><th>Added</th></tr>
{{ range $diff.Added }}
                                <tr><td>{{ . }}</td></tr>
{{ end }}
                            </table>
{{ end }}


{{ if $diff.Removed }}
                            <table class="table">
                                <tr><th>Removed</th></tr>
{{ range $diff.Removed }}
                                <tr><td>{{ . }}</td></tr>
{{ end }}
                            </table>
{{ end }}

{{ if $diff.Modified }}
                            <table class="table">
                                <tr><th>Modified</th></tr>
{{ range $modified, $modifiedDiff := $diff.Modified }}
                                <tr><td>{{ $modified }}
                                    <pre class="m-3 p-3 card card-warning"><small>{{ $modifiedDiff | base64 }}</small></pre>
                                </td></tr>
{{ end }}
                            </table>
{{ end }}
                        </div>
                    </div>
                </td></tr>
{{ end }}

            </tbody>
        </table>

{{ end }}

    </main>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
`
