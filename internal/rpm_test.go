package internal

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var fileProvider = []string{
	"maven-shared-utils-3.2.1-0.4.module_el8.2.0+364+b808f688.src.rpm",
	"389-ds-base-1.4.2.4-8.module_el8.2.0+366+71e3276f.src.rpm",
	"xmlstreambuffer-1.5.4-8.module_el8.3.0+454+67dccca4.src.rpm",
	"xsom-0-19.20110809svn.module_el8.2.0+315+896aef55.src.rpm",
	"xsom-0-19.20110809svn.module_el8.3.0+454+67dccca4.src.rpm",
	"varnish-modules-0.15.0-5.module_el8.3.0+399+10bd7ebd.src.rpm",
}

func TestRpm(t *testing.T) {
	for _, f := range fileProvider {
		rpm, _ := NewRpmType(f, "test")
		assert.Contains(t, rpm.Release, "module", rpm.FullPkgName)
	}

	rpm, _ := NewRpmType("apache-commons-codec-1.11-3.module+el8+2452+b359bfcd.src.rpm", "test")
	assert.True(t, rpm.IsModule)
}

// func TestNormalized(t *testing.T) {
//
// 	pkgList := []string{
// 		"test\\Cython-0.28.1-6.module+el8+2540+b19c9b35.src.rpm",
// 		"test\\Cython-0.28.1-7.module+el8.0.0+2961+596d0223.src.rpm",
// 		"test\\Cython-0.28.1-7.module+el8.0.0+4028+a686efca.src.rpm",
// 		"test\\Cython-0.28.1-7.module+el8.0.0.z+3358+99b46920.src.rpm",
// 		"test\\Cython-0.28.1-7.module+el8.1.0+3111+de3f2d8e.src.rpm",
// 		"test\\Cython-0.29.14-4.module+el8.2.0+5234+f98739b6.src.rpm",
// 	}
//
// 	srcList := []string {
// 		"Cython-0.28.1-7.module_el8.2.0+381+9a5b3c3b.src.rpm",
// 		"Cython-0.28.1-7.module_el8.3.0+577+674f770b.src.rpm",
// 		"Cython-0.29.14-4.module_el8.2.0+317+61fa6e7d.src.rpm",
// 		"Cython-0.29.14-4.module_el8.3.0+441+3b561464.src.rpm",
// 	}
//
// 	for _, f := range srcList {
// 		rpm, _ := NewRpmType(f, "test")
// 		_, _ = FindMatchingModuleVersion(*rpm, pkgList)
// 		// assert.NoError(t, err)
// 		// assert.Equal(t, "foobar", n)
// 	}
// }
