package internal

import (
	"errors"
	"fmt"
	"path/filepath"
	"strings"

	version "github.com/hashicorp/go-version"
)

// RpmType is a simple structure that holds information about an RPM FILENAME. This has nothing to do with the actual RPM contents.
type RpmType struct {
	FullPkgName string // Full name of the rpm
	Repo        string // Repository where this rpm is stored

	Name    string
	Version string
	Release string
	Epoch   string
	Arch    string

	IsSource bool // True when this pacakge is a source package (.src.rpm)
	IsModule bool // True when this package is a module ("module" in its release)
}

// Returns the normalized name of the package
func (rpm *RpmType) NormalizedModulePkgName() string {
	if !rpm.IsModule {
		return rpm.FullPkgName
	}

	// It's OK if this name isn't really a valid name, since we only use if for checking against other module names, not directly with filenames
	s := fmt.Sprintf("%s.%s.%s.MODULE", rpm.Name, rpm.Version, rpm.Release)
	return strings.Replace(s, "+", "_", -1)
}

// Taken and modified from: https://github.com/rpm-software-management/yum/blob/master/rpmUtils/miscutils.py
func NewRpmType(pkgName, repo string) (*RpmType, error) {
	rpm := &RpmType{
		FullPkgName: pkgName,
		Repo:        repo,
	}

	// Remove .rpm if available
	if strings.HasSuffix(pkgName, ".rpm") {
		pkgName = pkgName[:len(pkgName)-4]
	}

	// remove .src if available
	if strings.HasSuffix(pkgName, ".src") {
		pkgName = pkgName[:len(pkgName)-4]
		rpm.IsSource = true
	}

	// archIdx := strings.LastIndex(pkgName, ".")
	// rpm.Arch = pkgName[archIdx+1:]
	// // Sometimes we don't have a dot to denote the architecture (shim-15-11.src.rpm),
	// if archIdx == -1 {
	// 	archIdx = len(pkgName)
	// }
	archIdx := len(pkgName)

	relIdx := strings.LastIndex(pkgName[:archIdx], "-")
	if relIdx == -1 {
		// Seems there is no release
		relIdx = archIdx
	} else {
		rpm.Release = pkgName[relIdx+1 : archIdx]
	}

	rpm.IsModule = strings.Contains(rpm.Release, "module") || strings.Contains(rpm.Arch, "module")

	if rpm.IsModule {
		rpm.Release = strings.Replace(rpm.Release, "module+el8", "module_el8", 1)
		rpm.Arch = strings.Replace(rpm.Arch, "module+el8", "module_el8", 1)

		idx := strings.Index(rpm.Release, "+")
		if idx > 0 {
			rpm.Release = rpm.Release[:idx]
		}

		idx = strings.Index(rpm.Arch, "+")
		if idx > 0 {
			rpm.Arch = rpm.Arch[:idx]
		}
	}

	verIdx := strings.LastIndex(pkgName[:relIdx], "-")
	rpm.Version = pkgName[verIdx+1 : relIdx]

	epochIdx := strings.Index(pkgName, ":")
	if epochIdx > -1 {
		rpm.Epoch = pkgName[:epochIdx]
	}

	rpm.Name = pkgName[epochIdx+1 : verIdx]

	return rpm, nil
}

// IsHigherVersionThan can somewhat check if the current RPM is of a higher version (or release) than the given dst RPM.
func (rpm *RpmType) IsHigherVersionThan(dstRpm RpmType) bool {
	v1, err := version.NewVersion(rpm.Version)
	if err != nil {
		return false
	}
	v2, err := version.NewVersion(dstRpm.Version)
	if err != nil {
		return false
	}

	return v1.GreaterThan(v2)
}

// FindMatchingModuleVersion will try and find the closest matching module name from the given list of packages. Since
// modules cannot always be matched 1-1, a little more effort is needed to find a correct one.
func FindMatchingModuleVersion(rpm RpmType, pkgList []string) (string, error) {
	var candidates []string

	for _, tn := range pkgList {

		parts := strings.Split(tn, string(filepath.Separator))

		rhelRpm, err := NewRpmType(parts[1], parts[0])
		if err != nil {
			continue
		}

		if rhelRpm.NormalizedModulePkgName() == rpm.NormalizedModulePkgName() {
			candidates = append(candidates, rhelRpm.FullPkgName)
		}
	}

	// No candidates found
	if len(candidates) == 0 {
		return "", errors.New("not found")
	}

	// Only a single candidate found
	if len(candidates) == 1 {
		return candidates[0], nil
	}

	// // Multiple candidates found, try and fetch the correct one
	// for _, c := range candidates {
	// 	// @TODO: find the correct candidate
	// 	// fmt.Println("CAN: ", c)
	// }

	return candidates[len(candidates)-1], nil
}
