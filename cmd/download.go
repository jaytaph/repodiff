package cmd

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"gitlab.com/jaytaph/repodiff/internal"
	"gitlab.com/jaytaph/repodiff/internal/download"
)

var vault download.VaultType

var downloadCmd = &cobra.Command{
	Use:   "download",
	Short: "Download will download the C8 repositories locally",
	Run: func(cmd *cobra.Command, args []string) {
		vault = download.VaultType{
			BaseUrl: download.C8VaultUrl,
		}

		fmt.Println("Downloading indices...")
		indices, err := vault.GetIndices()
		if err != nil {
			fmt.Printf("Error while downloading indices: %s", err)
			os.Exit(1)
		}

		for repo, idxHtml := range indices {
			rpms := extractRpmsFromIndex(idxHtml)
			for rpmIdx, rpm := range rpms {
				fmt.Printf("[%4d/%4d] Downloading packages from '%s' repo            \r", rpmIdx+1, len(rpms), repo)
				p := internal.GetC8PkgPath(*dlRootDir, repo, rpm)

				_, ok := os.Stat(p)
				if ok != nil {
					// Download if not exists. Does not take partial downloads into account
					downloadPackage(*dlRootDir, repo, rpm)
				}
			}
			fmt.Printf("\n")
		}
	},
}

func extractRpmsFromIndex(idxHtml string) []string {
	rpmRegex := regexp.MustCompile("href=\"([^\"]+\\.rpm)\"")
	rpmFiles := rpmRegex.FindAllStringSubmatch(idxHtml, -1)

	var ret []string

	for _, rpmName := range rpmFiles {
		ret = append(ret, rpmName[1])
	}

	return ret
}

func downloadPackage(rootDir, repo, name string) {
	p := internal.GetC8PkgPath(rootDir, repo, name)

	found, pkgLen := vault.GetPackageInfo(repo, name)
	if found {
		info, err := os.Stat(p)
		if err == nil && info.Size() == pkgLen {
			// Already downloaded, and the correct size
			return
		}
	}

	fmt.Printf("\n   Downloading %s / %s \n", repo, name)
	r, err := vault.DownloadPackage(repo, name)
	defer func() {
		if r != nil {
			_ = r.Close()
		}
	}()

	if err != nil {
		fmt.Printf("%s package is not found (anymore)?\n", name)
		return
	}

	// Create file
	_ = os.MkdirAll(filepath.Dir(p), 0755)
	w, err := os.Create(p)
	if err != nil {
		fmt.Printf("%s error creating file: %s\n", name, err)
		return
	}
	defer func() {
		if w != nil {
			_ = w.Close()
		}
	}()

	// Write contents in file
	n, err := io.Copy(w, r)
	fmt.Printf("%s %d bytes written\n", name, n)
	if err != nil {
		fmt.Printf("%s err: %s\n", name, err)
	}
}

var (
	dlRootDir *string
)

func init() {
	rootCmd.AddCommand(downloadCmd)

	dlRootDir = downloadCmd.Flags().StringP("rootdir", "r", "", "root directory to hold your repositories")

	*dlRootDir, _ = homedir.Expand(*dlRootDir)

	_ = downloadCmd.MarkFlagRequired("rootdir")
}
