package cmd

import (
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"os"
	"time"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"gitlab.com/jaytaph/repodiff/internal"
	"gitlab.com/jaytaph/repodiff/internal/diff"
)

var genCmd = &cobra.Command{
	Use:   "generate",
	Short: "Generate a simple index.html file from the diff json output",
	Run: func(cmd *cobra.Command, args []string) {

		// Read JSON data
		buf, err := ioutil.ReadFile(*jsonPath)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		data := diff.JsonOutputType{}
		err = json.Unmarshal(buf, &data)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Load template
		tpl := template.Must(template.New("index").Funcs(template.FuncMap{
			"base64": func(data string) string {
				b, err := base64.StdEncoding.DecodeString(data)
				if err != nil {
					return ""
				}
				return string(b)
			},
			"sha": func(data string) string {
				b := sha1.Sum([]byte(data))
				return hex.EncodeToString(b[:])
			},
		}).Parse(internal.IndexTemplate))

		// Create output file, or use stdout if none has been given
		w := os.Stdout
		if *genOutputPath != "" {
			w, err = os.Create(*genOutputPath)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			defer func() {
				_ = w.Close()
			}()
		}

		// Generate output
		err = tpl.Execute(w, map[string]interface{}{
			"timestamp": time.Now().Format(time.RFC822),
			"repos":     data.Repos,
		})
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	},
}

var (
	jsonPath      *string
	genOutputPath *string
)

func init() {
	rootCmd.AddCommand(genCmd)

	jsonPath = genCmd.Flags().StringP("json", "j", "", "path to json file")
	genOutputPath = genCmd.Flags().StringP("output", "o", "", "output file for index.html")

	*jsonPath, _ = homedir.Expand(*jsonPath)
	*genOutputPath, _ = homedir.Expand(*genOutputPath)

	_ = genCmd.MarkFlagRequired("json")
}
