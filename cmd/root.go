package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "repodiff",
	Short: "RepoDiff is a simple rhel/c8 repository differ",
	Long: `RepoDiff is a simple rhel/c8 repository differ.
It can be used to diff source RPM packages between rhel and c8 and it can be used to automatically
download c8 packages from their public vault repositories.`,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		fmt.Println("")
		os.Exit(1)
	}
}
