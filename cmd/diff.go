package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"gitlab.com/jaytaph/repodiff/internal"
	"gitlab.com/jaytaph/repodiff/internal/diff"
)

// Global output for package diffs
var output diff.JsonOutputType

var diffCmd = &cobra.Command{
	Use:   "diff",
	Short: "Diff will diff rhel against c8 source packages",
	Run: func(cmd *cobra.Command, args []string) {
		// Sanity check if c8 paths exist
		p := internal.GetC8PkgDir(*diffRootDir, "baseos")
		_, ok := os.Stat(p)
		if ok != nil {
			fmt.Printf("C8 repo path not found in '%s'. Maybe you need to download the sources first with './rpmdiff download'?", *diffRootDir)
			os.Exit(1)
		}

		// Find packages for scanning module files when needed
		diff.RhelPkgList = internal.ScanRhelPackages(*diffRootDir, internal.RhelRepoDirs)

		// Initialize json output structure
		output = diff.NewJsonOutput()

		fmt.Printf("Starting differ with %d workers...\n", *numWorkers)
		diffSources(&output, *diffRootDir, *numWorkers)
	},
}

func diffSources(output *diff.JsonOutputType, rootDir string, numWorkers int) {
	// Rpm channel communicates all the rpms we need to diff to the workers
	rpmCh := make(chan internal.RpmType, numWorkers*10)

	// Push all found rpms to the rpm channel
	go func() {
		for _, repo := range internal.C8RepoDirs {
			processDir(rpmCh, internal.GetC8PkgPath(rootDir, repo, ""), repo)
		}
		// No more packages. CLose the channel so workers can can empty them
		close(rpmCh)
	}()

	// Set up workers that will actually do the processing per package.
	wg := new(sync.WaitGroup)
	for i := 0; i != numWorkers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			diff.PackageWorker(output, rootDir, rpmCh)
		}()
	}

	// Wait until all the workers have completed (ie: the rpm channel is emptied)
	wg.Wait()

	// And finally output our generated JSON file
	b, _ := json.MarshalIndent(output, "", "  ")
	if *diffOutputPath == "" {
		fmt.Println(string(b))
	} else {
		_ = ioutil.WriteFile(*diffOutputPath, b, 0644)
	}
}

// processDir will process a single directory for files, or recursively go into subdirectories if found
func processDir(wq chan internal.RpmType, p, repo string) {
	fl, err := ioutil.ReadDir(p)
	if err != nil {
		return
	}

	for _, f := range fl {
		if f.IsDir() {
			processDir(wq, filepath.Join(p, f.Name()), repo)
			continue
		}

		// Check if it's an RPM file
		if !strings.HasSuffix(f.Name(), ".rpm") {
			continue
		}

		rpm, err := internal.NewRpmType(f.Name(), repo)
		if err != nil {
			continue
		}

		// Add the RPM to the queue
		wq <- *rpm
	}
}

var (
	numWorkers     *int
	diffOutputPath *string
	diffRootDir    *string
)

func init() {
	rootCmd.AddCommand(diffCmd)

	diffRootDir = diffCmd.Flags().StringP("rootdir", "r", "", "root directory to hold your repositories")
	numWorkers = diffCmd.Flags().IntP("workers", "w", 10, "number of workers to run concurrently")
	diffOutputPath = diffCmd.Flags().StringP("output", "o", "", "output file for JSON")

	*diffRootDir, _ = homedir.Expand(*diffRootDir)
	*diffOutputPath, _ = homedir.Expand(*diffOutputPath)

	_ = diffCmd.MarkFlagRequired("rootdir")
}
