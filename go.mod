module gitlab.com/jaytaph/repodiff

go 1.13

require (
	github.com/hashicorp/go-version v1.2.1
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sassoftware/go-rpmutils v0.1.1
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/testify v1.6.1
)
