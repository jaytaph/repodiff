package main

import (
	"gitlab.com/jaytaph/repodiff/cmd"
)

func setupLogging() {
	// // Log to file and stdout
	// lf, err := os.OpenFile(fmt.Sprintf("log-%d.json", time.Now().Unix()), os.O_TRUNC|os.O_CREATE|os.O_RDWR, 0666)
	// if err != nil {
	// 	panic(err)
	// }
	// log.SetOutput(io.MultiWriter(lf, os.Stdout))
}

func main() {
	setupLogging()

	cmd.Execute()
}
