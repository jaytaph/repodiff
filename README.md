# RHEL / C8 Package source rpm differ

This system will compare RHEL8 and C8 packages against each other, and outputs the results. It can also be used for
downloading the publicly available C8 repositories.

### Requirements
  - you'll need the full source packages from RHEL8. 
    You can get them through the developer network of (https://developers.redhat.com/)[RedHat] and should be stored
    in your repositry root directory under `rhel8` directory.


### Usage
To download the C8 source RPMs from the public C8 vault:

     ./repodiff download --rootdir /my/dir

This will download the c8 source RPMs inside the root directory in the `c8` directory. 

To generate a diff between the RHEL8 and C8 SRPMs:

     ./repodiff diff --rootdir /my/dir -w 10 -o output.json

This will check the rhel sources against the c8 sources found in the rootdir. It will check with 10 workers (configurable through `-w`)


To generate an index.html:

    ./repodiff generate --json output.json -o index.html

This will generate a nice(ish) index.html file with the information.
